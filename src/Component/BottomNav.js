import {View, Text, Image} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../App/Home';
import History from '../App/History';
import Login from '../App/Login';

const Tab = createBottomTabNavigator();
const BottomNav = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false, //untuk menghilangkan label di bottom tab biar bisa styling sendiri text labelnya
      }}>
      {/* tab screen untuk ngasih alamat yang ada di bottom tab biar bisa dipakai */}
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarActiveTintColor: 'red',
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={{
                  uri: 'https://png.pngtree.com/png-vector/20191126/ourmid/pngtree-home-vector-icon-png-image_2036119.jpg',
                }}
                style={{resizeMode: 'contain', width: 20, height: 20}}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="History"
        component={History}
        options={{
          tabBarActiveTintColor: 'red',
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={{
                  uri: 'https://icones.pro/wp-content/uploads/2022/03/historique-icone-de-l-historique-noir.png',
                }}
                style={{resizeMode: 'contain', width: 20, height: 20}}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Login"
        component={Login}
        options={{
          tabBarActiveTintColor: 'red',
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={{
                  uri: 'https://png.pngtree.com/png-vector/20190411/ourmid/pngtree-vector-logout-icon-png-image_925699.jpg',
                }}
                style={{resizeMode: 'contain', width: 20, height: 20}}
              />
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomNav;

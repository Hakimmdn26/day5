import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';
import React from 'react';
import icBack from '../icons/back.png';

const GrabFood = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <View style={styles.box}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              onPress={() => {
                // navigation.navigate('HomeScreen');
                navigation.goBack();
                // goBack() adalah sebuah function dari si navigation di mana dia simpan navigasi history dan kembali ke screen 1
              }}>
              <View>
                <Image
                  source={icBack}
                  style={{
                    resizeMode: 'contain',
                    width: 50,
                    height: 50,
                    bottom: 10,
                  }}></Image>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <ScrollView>
        <View>
          <View>
            <View style={styles.row}>
              <View style={[styles.garis, styles.garis2]}></View>
              <View style={[styles.garis, styles.garis3]}></View>
              <View style={[styles.garis, styles.two]}></View>
            </View>
            <View style={styles.row}>
              <View style={[styles.garis, styles.two]}></View>
              <View style={[styles.garis, styles.garis2]}></View>
              <View style={[styles.garis, styles.garis3]}></View>
            </View>
            <View style={styles.row}>
              <View style={[styles.garis, styles.garis2]}></View>
              <View style={[styles.garis, styles.two]}></View>
              <View style={[styles.garis, styles.garis3]}></View>
            </View>
            <View style={styles.row}>
              <View style={[styles.garis, styles.garis2]}></View>
              <View style={[styles.garis]}></View>
              <View style={[styles.garis, styles.garis3]}></View>
            </View>
            <View style={styles.row}>
              <View style={[styles.garis, styles.garis2]}></View>
              <View style={[styles.garis]}></View>
            </View>
            <View style={styles.row}>
              <View style={[styles.garis]}></View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#2eb82e',
    // alignContent: 'center',
    // justifyContent:'center',
    // alignItems:'center'
  },
  box: {
    width: 0.1,
    height: 50,
    backgroundColor: 'white',
    top: 50,
    bottom: 50,
    marginBottom: 45,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  garis: {
    flex: 1,
    height: 100,
    backgroundColor: '#76BEFB',
  },
  garis2: {
    backgroundColor: '#A6E9FF',
  },
  garis3: {
    backgroundColor: '#FFDE7B',
  },
  two: {
    flex: 2,
  },
});

export default GrabFood;

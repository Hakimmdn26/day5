import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
  Platform,
  Image,
} from 'react-native';
// import BottomNav from '../Component/BottomNav';
// import fonts from './assets/fonts';

const History = ({navigation}) => {
  const [item, setItem] = useState(null);

  const dataHistory = async () => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    var raw = '';

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      requestOptions,
    )
      .then(response => response.text())
      .then(result => {
        const val = JSON.parse(result);
        setItem(Object.entries(val));
        console.log(result);
      })
      .catch(error => console.log('error', error));
  };
  useEffect(() => {
    dataHistory();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          backgroundColor: '#CCCC',
        }}>
        <View style={styles.header}>
          <Text style={styles.headerText}>History Transaksi</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.content}>
            {item?.map((e, index) => {
              return (
                <View
                  key={index}
                  style={{
                    borderWidth: 1,
                    borderColor: Platform.OS == 'ios' ? 'white' : null,
                    borderRadius: 10,
                    justifyContent: 'center',
                    margin: 5, //jarak antar kotak enter dgn input
                    padding: 4, //tinggi pendek kotak enter
                    width: 350, //panjang pendek kotak enter
                    backgroundColor: 'white', //background tulisan enter
                    // color:'white', //warna tulisan enter
                    overflow: 'hidden', //penyesuaian borderRadius ios
                    alignSelf: 'center',
                    borderBottomWidth: 5,
                    borderRightWidth: 4,
                    shadowColor: 'grey',
                    shadowOffset: {width: 1, height: 5},
                    shadowOpacity: 0.8,
                    shadowRadius: 3,
                    elevation: 3,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <View>
                      <Text
                        style={{
                          // fontStyle:'italic',
                          // fontWeight:'bold',
                          // fontFamily: 'TiltPrism-Regular',
                          fontSize: 16,
                        }}>
                        Sender : {e[0]}
                      </Text>
                      <Text
                        style={{
                          // fontFamily: 'Mynerve-Regular',
                          fontSize: 16,
                        }}>
                        Receiver : {e[1].target}
                      </Text>
                      <Text
                        style={
                          {
                            // fontFamily: 'Roboto-Bold', fontSize:14,
                          }
                        }>
                        Status : {e[1].type}
                      </Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          // fontFamily: 'Glossy Sheen Shadow DEMO',
                          // fontSize:14,
                        }}>
                        Rp.{e[1].amount}
                      </Text>
                    </View>
                    <Image
                      style={{
                        width: 40,
                        height: 40,
                        resizeMode: 'contain',
                      }}
                      source={{
                        uri: 'https://icons.veryicon.com/png/o/miscellaneous/small-green-icon/history-36.png',
                      }}
                    />
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7DFBB5',
  },
  header: {
    alignSelf: 'center',
    width: 350,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#33C072',
    borderRadius: 10,
    borderBottomWidth: 3,
    borderRightWidth: 2,
    shadowColor: 'grey',
    shadowOffset: {width: 1, height: 5},
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 3,
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  content: {
    color: 'black',
    padding: 10,
    shadowColor: '#171717',
    shadowOffset: {width: 2, height: 8},
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 3,
  },
});

export default History;

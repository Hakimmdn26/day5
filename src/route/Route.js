import {View, Text} from 'react-native';
import React from 'react';
import Home from '../App/Home';
import GrabFood from '../App/GrabFood';
import {createNativeStackNavigator} from '@react-navigation/native-stack'; // untuk membuat screen bisa di navigasi
import {Header} from 'react-native/Libraries/NewAppScreen';
import History from '../App/History';
import BottomNav from '../Component/BottomNav';

const Stack = createNativeStackNavigator();

function Route() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="BottomNav" component={BottomNav} />
      <Stack.Screen name="GrabFood" component={GrabFood} />
    </Stack.Navigator>
  );
}

export default Route;
